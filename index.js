'use strict'

module.exports = {
  extends: [
    'eslint-config-airbnb',
    '@instaffogmbh/eslint-config-instaffo',
  ].map(require.resolve),

  env: {
    browser: true,
    'jest/globals': true,
  },

  parser: require.resolve('babel-eslint'),
  parserOptions: {
    sourceType: 'module',
  },

  plugins: [
    "react",
    "jsx-a11y",
    "import",
    "babel",
    "jest",
  ],

  rules: {
    "max-len": ["error", 120],
    "comma-dangle": ["error", {
      "arrays": "always-multiline",
      "objects": "always-multiline",
      "imports": "always-multiline",
      "exports": "always-multiline",
      "functions": "never"
    }],
    "function-paren-newline": 0,
    "object-curly-newline": 0,
    "space-before-function-paren": ["error", "never"],
    "class-methods-use-this": 0,
    "jsx-a11y/no-autofocus": 0,
    "jsx-a11y/label-has-for": 0,
    "react/jsx-no-bind": 0,
    "react/no-array-index-key": 0,
    quotes: ["error", "single", {
      "allowTemplateLiterals": true,
      "avoidEscape": true
    }],
    "semi": ["error", "never"],
    "no-warning-comments": ["warn", { "terms": ["todo"], "location": "anywhere" } ],
    "jsx-a11y/no-static-element-interactions": 0,
    "jsx-a11y/click-events-have-key-events": 0,
    "jsx-a11y/mouse-events-have-key-events": 0,
    "jsx-a11y/anchor-is-valid": 0,
    "react/jsx-one-expression-per-line": 0,
    "react/destructuring-assignment": 0,
    "react/no-access-state-in-setstate": 0,
    "jsx-a11y/label-has-associated-control": 0,
    "import/no-unresolved": 0,
    "import/extensions": 0,
    "jsx-a11y/heading-has-content": 0,
    "no-restricted-globals": 0,
    "no-restricted-imports": ["error", {
      "patterns": ["../*", "./*"]
    }],
    "jest/no-disabled-tests": "warn",
    "jest/no-focused-tests": "error",
    "jest/no-identical-title": "error",
    "jest/no-jest-import": "error",
    "jest/no-large-snapshots": "error",
    "jest/prefer-to-have-length": "error",
    "jest/prefer-to-be-null": "error",
    "jest/prefer-to-be-undefined": "error",
    "jest/prefer-expect-assertions": "off",
    "jest/valid-expect": "error",
    "jest/valid-describe": "error",
    "jest/valid-expect-in-promise": "error",
    "no-multiple-empty-lines": ["error", { "maxEOF": 0, "max": 1, "maxBOF": 0 }],
    "react/jsx-curly-spacing": [2, {"when": "never", "children": true}],
    "react/prop-types": [2, { "ignore": ["strings"] }],
    "react/destructuring-assignment": ["error", "always"],
    "react/no-did-update-set-state": 0,
  },
}

